import Vue from "vue";
import Router from "vue-router";
import store from "@/store/store";

import WelcomePage from "./components/welcome/welcome.vue";
import DashboardPage from "./components/dashboard/dashboard.vue";
import SignupPage from "./views/auth/signup.vue";
import SigninPage from "./views/auth/signin.vue";
import Create from "./components/admin/createUser.vue";
import EditSurvey from "./components/survey/editSurvey.vue";
import Index from "./components/admin/index.vue";
import Profile from "./views/profile/profile.vue";
import CreateForm from "./components/forms/createForm.vue";
import CreateSurvey from "./components/survey/createSurvey.vue";
import Answers from "./components/survey/answers/answers.vue";
import Charts from "./components/survey/answers/charts.vue";

Vue.use(Router);

let router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    { path: "/", component: WelcomePage },
    { path: "/signup", name: "signup", component: SignupPage },
    { path: "/signin", component: SigninPage },
    {
      path: "/dashboard",
      component: DashboardPage,
      meta: { auth: true }
    },
    {
      path: "/test",
      name: "Test",
      component: () => import("./components/welcome/teste.vue")
    },
    {
      name: "Profile",
      path: "/profile",
      component: Profile,
      meta: { auth: true }
    },
    {
      name: "Create",
      path: "/create",
      component: Create,
      meta: { auth: true }
    },
    {
      name: "EditSurvey",
      path: "/editsurvey/:id",
      component: EditSurvey,
      meta: { auth: true }
    },
    {
      name: "Index",
      path: "/index",
      component: Index,
      meta: { auth: true }
    },
    {
      name: "CreateForm",
      path: "/createform",
      component: CreateForm,
      meta: { auth: true }
    },
    {
      name: "CreateSurvey",
      path: "/createSurvey",
      component: CreateSurvey,
      meta: { auth: false }
    },
    {
      name: "Answers",
      path: "/survey/answers/:id",
      component: Answers,
      meta: { auth: true }
    },
    {
      name: "Charts",
      path: "/survey/answers/chart",
      component: Charts,
      meta: { auth: true }
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.meta.auth && !store.state.currentUser) {
    next({
      path: "/signin"
    });
  }
  next();
});

export default router;
