import axios from "axios";
import store from "@/store/store";
import databaseAuth from "@/services/databaseAuth";
import router from "../router";

const instance = axios.create({
  baseURL: "http://localhost:8080"
});

axios.defaults.headers["Access-Control-Allow-Credentials"] = true;

/*
 * The interceptor here ensures that we check for the token in local storage every time an instance request is made
 */

instance.interceptors.request.use(
  config => {
    let token = store.getters.getToken;

    if (token) {
      config.headers["Access-Token"] = `${token}`;
    }
    return config;
  },

  error => {
    return Promise.reject(error);
  }
);

instance.interceptors.response.use(null, error => {
  if (error.response.status) {
    if (error.response.status === 401) {
      databaseAuth.signOut();
      router.push("/signin");
    }
    return Promise.reject(error);
  }
});

export default instance;
