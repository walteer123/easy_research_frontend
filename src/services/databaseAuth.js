import firebase from "firebase";
import store from "@/store/store";

// Initialize Firebase
var config = {
  apiKey: "AIzaSyDdaUQjts295rbFNmu5drWtx3uuArJksZ4",
  authDomain: "equestauth.firebaseapp.com",
  databaseURL: "https://equestauth.firebaseio.com",
  projectId: "equestauth",
  storageBucket: "equestauth.appspot.com",
  messagingSenderId: "981771067800"
};

const databaseAuth = firebase.initializeApp(config);

databaseAuth.signUp = async payload => {
  try {
    await firebase
      .auth()
      .createUserWithEmailAndPassword(payload.email, payload.password);
    let user = firebase.auth().currentUser;
    user.updateProfile({
      displayName: payload.userName
    });
    store.commit("setCurrentUser", firebase.auth().currentUser);
    store.commit("setUserID", firebase.auth().currentUser.uid);
    return true;
  } catch (error) {
    return error;
  }
};

databaseAuth.signIn = async payload => {
  try {
    await firebase
      .auth()
      .signInWithEmailAndPassword(payload.email, payload.password);
    store.commit("setCurrentUser", firebase.auth().currentUser);
    store.commit("setUserID", firebase.auth().currentUser.uid);
    return true;
  } catch (error) {
    return error;
  }
};

databaseAuth.saveToken = (callback) => {
   firebase
    .auth()
    .currentUser.getIdToken(true)
    .then(idToken => {
      console.log(idToken);
      localStorage.setItem("token", idToken);
      store.commit("setToken", idToken);
      callback();
    })
    .catch(function(error) {
      console.log(error);
    });
};

databaseAuth.signOut = async () => {
  try {
    await firebase.auth().signOut();
    store.commit("setCurrentUser", null);
    store.commit("setToken", null);
    return true;
  } catch (error) {
    return error;
  }
};

databaseAuth.editUser = async payload => {
  try {
    let user = firebase.auth().currentUser;

    user.updateProfile({
      displayName: payload.userName
    });

    user.updateEmail(payload.email);

    store.commit("setCurrentUser", firebase.auth().currentUser);
    return true;
  } catch (error) {
    return error;
  }
};

databaseAuth.saveUserId = () => {
  return firebase.auth().currentUser.uid;
};

export default databaseAuth;
