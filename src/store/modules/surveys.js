import axios from "../../services/axios";

const state = {
  createSurvey: null,
  currentSurvey: null
};

const mutations = {
  storeForm(state, payload) {
    state.createSurvey = payload;
  },
  storeCurrentForm(state, payload) {
    state.currentSurvey = payload;
  }
};

const actions = {
  createSurvey({ commit }, payload) {
    return new Promise(resolve => {
      resolve(axios.post("/survey", payload));
    });
  },
  getAllSurveysByUser({ commit }, userId) {
    axios
      .get("/survey/getAll/createdBy", userId)
      .then(res => {
        commit("storeCurrentSurvey", res);
        console.log(res);
      })
      .catch(error => console.log(error));
  },
  createAnswers({ commit }, payload) {
    return new Promise(resolve => {
      resolve(axios.post("/answer/addAll", payload));
    });
  }
};

const getters = {};

export default {
  state,
  mutations,
  actions,
  getters
};
