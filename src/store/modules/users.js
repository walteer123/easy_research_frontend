import axios from "../../services/axios";
import user from "../../models/User";

const state = {
  user
};

const mutations = {
  storeUser(state, user) {
    state.user = user;
  }
};

const actions = {
  storeUser({ state }, payload) {
    axios
      .post("/user", payload, state.header)
      .then(res => console.log(res))
      .catch(error => console.log(error));
  }
};

const getters = {};

export default {
  state,
  mutations,
  actions,
  getters
};
