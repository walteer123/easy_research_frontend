/* eslint-disable no-unused-vars */
import axios from "../../services/axios";
import { reject } from "q";
import store from "../../store/store";
import auth from "../../services/databaseAuth";

const templateURL = "/template";
const questionURL = "/question";
const answerURL = "/answer";

const state = {
  createForm: null,
  currentForm: null,
  formsByUser: null,
  questionsArray: null,
  retrievedForm: null
};

const mutations = {
  storeForm(state, payload) {
    state.createForm = payload;
  },
  storeCurrentForm(state, payload) {
    state.currentForm = payload;
  },
  storeQuestionsArray(state, payload) {
    state.questionsArray = payload;
  },
  currentRetrievedForm(state, payload) {
    state.retrievedForm = payload;
  },
  storeFormsByUser(state, payload) {
    state.formsByUser = payload;
  }
};

const actions = {
  createQuestions({ commit }, payload) {
    return new Promise((resolve, reject) => {
      resolve(axios.post(`${questionURL}/addAll`, payload));
    }).catch(error => console.log(error));
  },

  createForm({ commit }, payload) {
    return new Promise((resolve, reject) => {
      resolve(axios.post(templateURL, payload));
    }).catch(error => console.log(error));
  },

  getFormsByUser() {
    let userid = auth.saveUserId();
    return new Promise((resolve, reject) => {
      resolve(
        axios.get(
          `${templateURL}/getAll/createdBy/${userid}?offset=0&limit=9999999`
        )
      );
    });
  },

  getFormById({ commit }, formId) {
    axios
      .get("/template/getBy/id/" + formId)
      .then(res => {
        commit("currentRetrievedForm", res);
        localStorage.setItem("retrievedForm", JSON.stringify(res));
      })
      .catch(error => console.log(error));
  },

  deleteForm({ commit }, payload) {
    axios
      .delete(templateURL, { data: payload })
      .then(res => {
        console.log(res);
        console.log(payload);
      })
      .catch(error => {
        console.log(error);
      });
  },

  editForm({ commit }, payload) {
    return new Promise((resolve, reject) => {
      resolve(axios.put(templateURL, payload));
    });
  },

  findFormById({ commit }, payload) {
    return new Promise((resolve, reject) => {
      resolve(axios.get(`${templateURL}/getBy/id/${payload}`, payload));
    }).catch(error => console.log(error));
  },

  getAnswers({ commit }, payload) {
    return new Promise((resolve, reject) => {
      resolve(axios.post(`${answerURL}/getAllBy/question`, payload));
    }).catch(error => console.log(error));
  },

  getAnswersById({ commit }, payload) {
    return new Promise((resolve, reject) => {
      resolve(
        axios.post(`${answerURL}/getAllBy/question/{questionId}${payload}`)
      );
    }).catch(error => console.log(error));
  }
};

const getters = {};

export default {
  state,
  mutations,
  actions,
  getters
};
