import Vue from "vue";
import Vuex from "vuex";

import users from "./modules/users";
import forms from "./modules/forms";
import surveys from "./modules/surveys";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currentUser: null,
    token: localStorage.getItem("token") || "",
    userID: null
  },
  mutations: {
    setCurrentUser(state, payload) {
      state.currentUser = payload;
    },
    setToken(state, payload) {
      state.token = payload;
    },
    setUserID(state, payload) {
      state.userID = payload;
    }
  },
  getters: {
    getToken: state => {
      return state.token;
    }
  },
  modules: {
    users,
    forms,
    surveys
  }
});
