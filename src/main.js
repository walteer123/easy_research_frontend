import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store/store";
import firebase from "firebase";
import BootstrapVue from "bootstrap-vue";
import Vuelidate from "vuelidate";
import Chartkick from "vue-chartkick";
import Chart from "chart.js";
import excel from "vue-excel-export";

import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "@/assets/style/styles.scss";
import Loading from "vue-loading-overlay";
import "vue-loading-overlay/dist/vue-loading.css";

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(Vuelidate);
Vue.use(Chartkick.use(Chart));
Vue.use(excel);
Vue.use(Loading);

let app;

const initialize = () => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount("#app");
  }
};

firebase.auth().onAuthStateChanged(user => {
  if (user) {
    store.commit("setCurrentUser", user);
  } else {
    store.commit("setCurrentUser", null);
  }

  initialize();
});
