class Survey {
  constructor(name, description, answers, template) {
    (this.name = name),
      (this.description = description),
      (this.answers = answers),
      (this.template = template);
  }
}

module.exports = {
  Survey
};
